#include "material_point.hh"

/* -------------------------------------------------------------------------- */
void MaterialPoint::printself(std::ostream& stream) const {
  stream << " " << temperature << " " << heat_rate << " " << heat_source << " "<< position;
}

/* -------------------------------------------------------------------------- */

void MaterialPoint::initself(std::istream& sstr) {
  sstr >> temperature  >> heat_rate >> heat_source >> position;;
}
