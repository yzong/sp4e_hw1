#include "compute_temperature.hh"
#include "csv_reader.hh"
#include "csv_writer.hh"
#include "material_point.hh"
#include "material_points_factory.hh"
#include "system.hh"
#include <gtest/gtest.h>

/*****************************************************************/
// Test case 1: homogeneous temperature
class HomoTemp : public ::testing::Test {
protected:
  void SetUp() override {
    MaterialPointsFactory::getInstance();
    std::vector<MaterialPoint> material_points;
    n_grid = 8;
    n_points = n_grid * n_grid;
    for (UInt i = 0; i < n_points; ++i) {
      MaterialPoint p;
      p.getPosition()[0] = i % n_grid;
      p.getPosition()[1] = i / n_grid; 
      p.getTemperature() = 0.;
      p.getHeatRate() = 0.;
      p.getHeatSource() = 0.;
      material_points.push_back(p);
    }

    for (auto& p : material_points) {
      system.addParticle(std::make_shared<MaterialPoint>(p));
    }
  }

  System system;
  UInt n_points;
  UInt n_grid;
};

TEST_F(HomoTemp, zeroT) {
  Real dt = 1;
  Real rho = 1;
  Real C = 1;
  Real k = 1;
  Real dx = 1;

  ComputeTemperature c(dt, rho, C, k, dx);

  c.compute(system);

  for (UInt i = 0; i < n_points; i++){
    Particle& par = system.getParticle(i);   
    MaterialPoint& mat_point = static_cast<MaterialPoint&>(par);
    auto pos = par.getPosition();
    Real x = pos[0];
    Real y = pos[1];
    ASSERT_NEAR(mat_point.getTemperature(), 0, 1e-15);
  }

}

/***************************************,**************************/
// Test case 2: sinusoidal volumertic heat source
class SinHeatSource : public ::testing::Test {

protected:
  // specific case for x, y in [-1,1], L=2
  double heat_source(double x, double y) {
    return 4 * M_PI * M_PI / 4 / 4 * sin (2 * M_PI * x / 2 / 2);
  }

  double steady_state_temperature (double x, double y) {
    return sin (2 * M_PI * x / 2 / 2);
  }

  double get_x (UInt N, UInt idx) {
    return 2. * (double)(idx % N) / N - 1.;
  }

  double get_y (UInt N, UInt idx) {
    return 2. * (double)(idx / N) / N - 1..;
  }

  void SetUp() override {
    MaterialPointsFactory::getInstance();
    std::vector<MaterialPoint> material_points;
    
    n_grid = 8;
    n_points = n_grid * n_grid;

    for (UInt i = 0; i < n_points; ++i) {
      MaterialPoint p;
      double x = get_x(n_grid, i);
      double y = get_y(n_grid, i);
      p.getPosition()[0] = x;
      p.getPosition()[1] = y;   
      p.getTemperature() = steady_state_temperature(x, y);
      p.getHeatRate() = 0.;
      p.getHeatSource() = heat_source(x, y);
      
      material_points.push_back(p);
    }

    for (auto& p : material_points) {
      system.addParticle(std::make_shared<MaterialPoint>(p));
    }

  }
  System system;
  UInt n_grid;
  UInt n_points;
};

TEST_F(SinHeatSource, steadystate) {

  Real dt = 1;
  Real rho = 1;
  Real C= 1;
  Real k = 1; 
  Real Deltax = 2./n_grid;

  ComputeTemperature c(dt, rho, C, k, Deltax);

  c.compute(system);

  for (UInt i = 0; i < n_points; i++){
    Particle& par = system.getParticle(i);   
    MaterialPoint& mat_point = static_cast<MaterialPoint&>(par);
    auto pos = par.getPosition();
    Real x = pos[0];
    Real y = pos[1];
    // std::cout << "x=" << x << ", y=" << y << ", Treal=" << mat_point.getTemperature() << ", Tss=" << steady_state_temperature(x,y) << std::endl;
    ASSERT_NEAR(mat_point.getTemperature(), steady_state_temperature(x, y), 1e-15);
  }

}

/*****************************************************************/
// Test case 3: delta heat source

class DeltaHeatSource : public ::testing::Test {

protected:
  double heat_source(double x, double y) {
    if (x == 0.5) {
      return 1;
    }
    else if (x == -0.5) { 
      return -1;
    }
    else {
      return 0;
    }
  }

  double steady_state_temperature (double x, double y) {
    if (x <= -0.5) {
      return - x - 1;
    }
    else if (x <== 0.5) {
      return x;
    }
    else {
      return - x + 1;
    }
  }
  
  double get_x (UInt N, UInt idx) {
    return 2. * (double)(idx % N) / N - 1.;
  }

  double get_y (UInt N, UInt idx) {
    return 2. * (double)(idx / N) / N - 1;
  }


  void SetUp() override {
    MaterialPointsFactory::getInstance();
    std::vector<MaterialPoint> material_points;

    n_grid = 8;
    n_points = n_grid * n_grid;

    for (UInt i = 0; i < n_points; ++i) {
      MaterialPoint p;
      double x = get_x(n_grid, i);
      double y = get_y(n_grid, i);
      p.getPosition()[0] = x;
      p.getPosition()[1] = y;
      p.getTemperature() = steady_state_temperature(x, y);
      p.getHeatRate() = 0.;
      p.getHeatSource() = heat_source(x, y);
      material_points.push_back(p);
    }

    for (auto& p : material_points) {
      system.addParticle(std::make_shared<MaterialPoint>(p));
    }

  }
  System system;
  UInt n_grid;
  UInt n_points;
};

TEST_F(DeltaHeatSource, steadystate) {
  Real dt = 1;
  Real rho = 1;
  Real C= 1;
  Real k = 1; 
  Real Deltax = 2./n_grid;

  ComputeTemperature c(dt, rho, C, k, Deltax);
  c.compute(system);

  for (UInt i = 0; i < n_points; i++){
    Particle& par = system.getParticle(i);   
    MaterialPoint& mat_point = static_cast<MaterialPoint&>(par);
    auto pos = par.getPosition();
    Real x = pos[0];
    Real y = pos[1];
    ASSERT_NEAR(mat_point.getTemperature(), steady_state_temperature(x, y), 1e-15);
  }
}