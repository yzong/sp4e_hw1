#ifndef __COMPUTE_TEMPERATURE__HH__
#define __COMPUTE_TEMPERATURE__HH__

/* -------------------------------------------------------------------------- */
#include "compute.hh"

//! Compute temperature evolution
class ComputeTemperature : public Compute {

public: 
ComputeTemperature(Real timestep, Real density, Real heat_capacity, 
                                Real heat_conduction, Real scaling);

  // Virtual implementation
public:
  //! Temperature evolution
  void compute(System& system) override;
  // Simulation parameters 
  void setDeltaT (Real dt);
  void setDensity (Real rho);
  void setCapacity (Real C);
  void setConduction (Real k);
  void setScaling (Real Deltax);

private:
Real dt, rho, C, k, Deltax;
};

/* -------------------------------------------------------------------------- */
#endif  //__COMPUTE_TEMPERATURE__HH__
