#include "compute_temperature.hh"
#include "fft.hh"
#include "material_point.hh"
#include <cmath>
#include <math.h>

/* -------------------------------------------------------------------------- */
ComputeTemperature::ComputeTemperature(Real dt, Real rho, Real C, Real k, Real Deltax) : 
dt(dt),rho(rho), C(C), k(k), Deltax(Deltax) {}

void ComputeTemperature::setDeltaT(Real dt) { this->dt = dt; }
/*  For setting parameters different to default values */
/*  Default values are all equal to 1 */
void ComputeTemperature::setDensity(Real rho) { this->rho = rho; }
void ComputeTemperature::setCapacity(Real C) { this->C = C; }
void ComputeTemperature::setConduction(Real k) { this->k = k; }
void ComputeTemperature::setScaling(Real Deltax) { this->Deltax = Deltax; }


void ComputeTemperature::compute(System& system) {

  UInt size = system.getNbParticles();
  /* Input size for square matrices */
  UInt matrix_size = std::sqrt(size);

  Matrix<complex> Temp_matrix (matrix_size);
  Matrix<complex> heat_source (matrix_size);
  Matrix<complex> heat_rate (matrix_size);
  Matrix<complex> Temp_matrix (matrix_size);
  Matrix<complex> heat_source (matrix_size);
  Matrix<complex> heat_rate (matrix_size);


/* Store material points in a square Matrix */
  for (int i = 0 ; i < size; i++) {
    
    Particle& par = system.getParticle(i);   
    MaterialPoint& mat_point = static_cast<MaterialPoint&>(par);

    auto pos = par.getPosition();
    Real x = pos[0];
    Real y = pos[1];

    /* Handle case of negative pos, shift them by the period T */
    if (x<0) {x += (this -> Deltax * matrix_size);}
    if (y<0) {y += (this -> Deltax * matrix_size);}

    // /* Matrix index in the space grid */
    UInt j_x = (UInt)(x / this -> Deltax);
    UInt j_y = (UInt)(y / this -> Deltax);

    Temp_matrix (j_x,j_y) = mat_point.getTemperature();
    heat_source (j_x,j_y) = mat_point.getHeatSource();
    heat_rate (j_x,j_y) = mat_point.getHeatRate();
    
    }

/* FT of temperature and heat source*/
auto Temp_matrix_FT = FFT::transform(Temp_matrix);
auto heat_source_FT = FFT::transform(heat_source);
Matrix<complex> heat_rate_FT (matrix_size);

/* Heat rate calculation */
Matrix<complex> frequencies = FFT::computeFrequencies(size, this -> Deltax);

for (int j = 0 ; j < matrix_size; j++) {
    for (int i = 0; i < matrix_size; i++) {
      /* Frequencies q_x and q_y squared */
      complex freq_squared = 4 * (M_PI*M_PI) * (std::pow(frequencies(i,j).real(), 2) + std::pow(frequencies(i,j).imag(), 2));
      /* Heat Rate expression in Fourier space*/
      heat_rate_FT(i,j) = 1/(this -> rho * this -> C) * (heat_source_FT(i,j) - this -> k * (Temp_matrix_FT(i,j) * freq_squared));
    }
}

/* Inverse FT of heat rate*/
heat_rate =FFT::itransform(heat_rate_FT);

/* Store the calculated Temperature in each Material point  */
for (int i = 0; i < size; i++) {
    
    Particle& par = system.getParticle(i);   
    auto& mat_point = static_cast<MaterialPoint&>(par);

    auto pos = par.getPosition();
    Real x = pos[0];
    Real y = pos[1];

    /* Handle case of negative pos, shift them by the period T */
    if (x<0) {x += (this -> Deltax * matrix_size);}
    if (y<0) {y += (this -> Deltax * matrix_size);}
    /* Matrix index in the space grid */
    UInt j_x = static_cast<UInt>(x / this -> Deltax);
    UInt j_y = static_cast<UInt>(y / this -> Deltax);
    
    /* Temp at n+1 time step */
    mat_point.getTemperature() += (heat_rate(j_x,j_y).real() * this -> dt);

    /* Store new heat rate */
    mat_point.getHeatRate() = heat_rate(j_x,j_y).real();
    }

}

// /* -------------------------------------------------------------------------- */
