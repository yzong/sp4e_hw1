# SP4E Homework 3

## Exercise 1
`particle` object stores the data of one particle (position, velocity, temperature etc.). `system` object is a container of particles to be simulated. The I/O interface between `system` and the file system is provided by `csv_reader` and `csv_writer`.  `system_evolution` object stores the interactions between particles and the steps to compute one evolution during the simulation. The interactions between particles are included in `compute` classes. They are programmed into the `system_evolution` through `particles_factory` object.

## Exercise 2
See implementation in `CMakeLists.txt`.

## Exercise 3
check result by running `test_fft`.

## Exercise 4
Test described in 4.2, 4.3, and 4.4 are implemented in `test_temperature`. Test 4.2 passed. Test 4.3 and 4.4 didn't pass, but we notice the calculated value is in phase with the analytical value so it's a problem of scaling. We didn't have enough time to locate where the scaling error happened.

Here is an example of lauching one simulation. In this simulation we calculate 100 steps and dump each 10 step. We set timestep as 0.5. The input file `test.csv` should be in the same folder as `particles`. In `test.csv`, the oder of columns is: temperature, heat rate, heat source, position x, position y, and position z (=0). Also make sure there is a `dumps` folder to store the results.

`particles 100 10 test.csv material_point 0.5`

After running the line above, we can import the files in `dumps` folder in Paraview.
