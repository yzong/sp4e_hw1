#ifndef FFT_HH
#define FFT_HH
/* ------------------------------------------------------ */
#include "matrix.hh"
#include "my_types.hh"
#include <fftw3.h>
/* ------------------------------------------------------ */

struct FFT {

  static Matrix<complex> transform(Matrix<complex>& m);
  static Matrix<complex> itransform(Matrix<complex>& m);

  static Matrix<complex> computeFrequencies(int size, Real Deltax);
};

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::transform(Matrix<complex>& m_in) {
    fftw_complex *in, *out;
    fftw_plan plan;
    
    UInt N = m_in.size();
    Matrix<complex> m_out (N);
    
    /* need to cast into fftw_complex */
    in = reinterpret_cast<fftw_complex*> (m_in.data());
    out = reinterpret_cast<fftw_complex*> (m_in.data());
    plan = fftw_plan_dft_2d(N, N, in, out, FFTW_FORWARD, FFTW_ESTIMATE);

    fftw_execute(plan);

    fftw_destroy_plan(plan);

    return m_in; 
}

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::itransform(Matrix<complex>& m_in) {
    
    fftw_complex *in, *out;
    fftw_plan p;
    UInt N = m_in.size();
    
    /* need to cast into fftw_complex */
    in = reinterpret_cast<fftw_complex*> (m_in.data());
    out = reinterpret_cast<fftw_complex*> (m_in.data());

    p = fftw_plan_dft_2d(N, N, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);

    fftw_execute(p); /* repeat as needed */

    fftw_destroy_plan(p);

    /* scale the inverse by N */
    double norm = N*N ;
    m_in /= norm ;

    return m_in;
}

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::computeFrequencies(int size, Real Deltax) {
  Matrix<complex> frequencies(size);
    
    /* loop optimized for column order matrix */
    for (int j = 0; j < size; j++) {
      for (int i = 0; i < size; i++) {
        /* shifted frequencies */
        int freqX = i < (size+1) / 2 ? i : i - size;
        int freqY = j < (size+1) / 2 ? j : j - size;
        /* stored as complex number, real part is the x and imag part is y*/
        /* Scaling by T = (N * dx) taken into account */
        frequencies(i,j) = {freqX / (size * Deltax), freqY / (size* Deltax) } ;
      }
    }

return frequencies;

}



#endif  // FFT_HH 

