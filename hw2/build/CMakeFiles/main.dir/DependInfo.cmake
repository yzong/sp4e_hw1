
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/Users/luca/SP_course/sp4e_hw1/hw2/src/ComputeArithmetic.cc" "CMakeFiles/main.dir/src/ComputeArithmetic.cc.o" "gcc" "CMakeFiles/main.dir/src/ComputeArithmetic.cc.o.d"
  "/Users/luca/SP_course/sp4e_hw1/hw2/src/ComputePi.cc" "CMakeFiles/main.dir/src/ComputePi.cc.o" "gcc" "CMakeFiles/main.dir/src/ComputePi.cc.o.d"
  "/Users/luca/SP_course/sp4e_hw1/hw2/src/DumperSeries.cc" "CMakeFiles/main.dir/src/DumperSeries.cc.o" "gcc" "CMakeFiles/main.dir/src/DumperSeries.cc.o.d"
  "/Users/luca/SP_course/sp4e_hw1/hw2/src/PrintSeries.cc" "CMakeFiles/main.dir/src/PrintSeries.cc.o" "gcc" "CMakeFiles/main.dir/src/PrintSeries.cc.o.d"
  "/Users/luca/SP_course/sp4e_hw1/hw2/src/RiemannIntegral.cc" "CMakeFiles/main.dir/src/RiemannIntegral.cc.o" "gcc" "CMakeFiles/main.dir/src/RiemannIntegral.cc.o.d"
  "/Users/luca/SP_course/sp4e_hw1/hw2/src/WriteSeries.cc" "CMakeFiles/main.dir/src/WriteSeries.cc.o" "gcc" "CMakeFiles/main.dir/src/WriteSeries.cc.o.d"
  "/Users/luca/SP_course/sp4e_hw1/hw2/src/main.cc" "CMakeFiles/main.dir/src/main.cc.o" "gcc" "CMakeFiles/main.dir/src/main.cc.o.d"
  "/Users/luca/SP_course/sp4e_hw1/hw2/src/series.cc" "CMakeFiles/main.dir/src/series.cc.o" "gcc" "CMakeFiles/main.dir/src/series.cc.o.d"
  )

# Targets to which this target links which contain Fortran sources.
set(CMAKE_Fortran_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
