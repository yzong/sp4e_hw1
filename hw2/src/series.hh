#ifndef SERIES_HH
#define SERIES_HH

class Series{
    
    public:
        Series();
        virtual ~Series();
    
    public:
        unsigned int current_index;
        double current_value;
        // pure virtual
        // virtual double compute(unsigned int N) = 0;
        // virtual
        virtual double compute(unsigned int N);
        virtual void add_term(void);
        virtual double computeTerm(unsigned int k) = 0;
        virtual double getAnalyticPrediction(void);

};

#endif