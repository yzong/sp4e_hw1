#ifndef RIEMANNINTEGRAL_HH
#define RIEMANNINTEGRAL_HH
#include "series.hh"
#include <functional>

class RiemannIntegral: public Series{

    public:
    RiemannIntegral(std::function<double(double)> func, double a, double b);
    virtual ~RiemannIntegral();
    double computeTerm(unsigned int k) override;
    double compute(unsigned int N) override;

    protected:
    std::function<double(double)> f;
    double a;
    double b;
    double dx;

    double computeTerm(unsigned int k) override;
    double compute(unsigned int N) override;
};

#endif