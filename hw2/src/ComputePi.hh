#ifndef COMPUTEPI_HH
#define COMPUTEPI_HH
#include "series.hh"

class ComputePi: public Series{

    public:
    ComputePi();
    virtual ~ComputePi();

    // double compute(unsigned int N);
    double getAnalyticPrediction(void) override;
    double computeTerm(unsigned int k) override;
    double compute(unsigned int N) override;
};

#endif