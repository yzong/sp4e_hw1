import matplotlib.pyplot as plt
import argparse

parser = argparse.ArgumentParser(
    description='Plot file results'
)

parser.add_argument('--filename', type=str, help="filename")
parser.add_argument('--separator', type = str, help='type of separator')

args = parser.parse_args()
filename = args.filename
separator = args.separator

numerical = []
analytic = []
x = []

with open (filename) as f:
    for line in f:
        data = line.strip().split(separator)
        x.append(data[0])
        numerical.append(data[1])
        #check if the analytical prediction is present or not
        if len(data) >= 3:
            analytic.append(data[2])
        

fig = plt.figure()
axe = fig.add_subplot(1, 1, 1)


axe.plot(x, numerical, marker='o', label='Numerical')
if (analytic):
    axe.plot(x, analytic, label='Analytical')
axe.set_xlabel(r'$k$')
axe.set_ylabel(r'Series')
axe.legend()

plt.show()

