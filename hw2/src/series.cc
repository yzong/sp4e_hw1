# include "series.hh"
# include <math.h>

Series::Series(){}
Series::~Series(){}


double Series::compute(unsigned int N){
    if (this -> current_index <= N){
        N -= this -> current_index;
    }
    else{
        this -> current_value = 0;
        this -> current_index = 0;
    }

    for (unsigned int k=1; k<=N; k++){
        this -> add_term();
    }
    return this -> current_value;
}

void Series::add_term(void){
    this -> current_index += 1;
    this -> current_value += this -> computeTerm(this -> current_index);
}

double Series::getAnalyticPrediction(void){
    return nan("");
}

