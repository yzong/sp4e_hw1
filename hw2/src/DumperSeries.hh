#ifndef DUMPERSERIES_HH
#define DUMPERSERIES_HH

#include <iostream>
#include "series.hh"


class DumperSeries {

    public:

        DumperSeries(Series &s);
        // how to be sure that series = s ??
        virtual ~DumperSeries();

        virtual void dump(std::ostream & os) = 0 ;
        //method for specyfing precision of series
        void setPrecision(unsigned int newprecision);


    protected:
        Series & series ;
        unsigned int precision ;

    };

// rewrite operator declaration 
inline std::ostream & operator <<(std::ostream & stream, DumperSeries & _this) { _this.dump(stream);
return stream;
}


#endif