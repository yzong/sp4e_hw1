#ifndef WRITESERIES_HH
#define WRITESERIES_HH

#include "DumperSeries.hh"
#include <iostream>
#include <string>
#include <fstream>

class WriteSeries: public DumperSeries {
    public: 
        WriteSeries(Series &series, int freq, int maxit, std::string  &file);
        virtual ~WriteSeries();

        void dump(std::ostream & os = std::cout) override ;

        void setSeparator(char separator);  
    
    private: 
        int frequency ;
        int maxiter;
        std::string &filename;
        char separator = ' '; 

};

#endif 