#include <iostream>
#include <sstream>
#include <fstream>
#include "ComputeArithmetic.hh"
#include "ComputePi.hh"
#include "RiemannIntegral.hh"
#include "PrintSeries.hh"
#include "WriteSeries.hh"
#include "DumperSeries.hh"
#include <functional>
#include <math.h>
#include <string>
#include <cstring>
#include <cstdlib>
#include <memory>

int main(int argc, char* argv[]){

    std::stringstream sstr;
    for (int i = 1; i < argc; ++i) {
    sstr << argv[i] << " ";
    }

    // parse which type of series to compute
    std::string series_type ;
    sstr >> series_type;
    
    // print or write a file
    std::string dumper_type;
    sstr >> dumper_type;

    // parse series type and store in series pointer
    Series *ptr = nullptr;
    if (series_type.compare("Arithmetic")==0){
        ptr = new ComputeArithmetic();
    }
    else if (series_type.compare("Pi")==0){
        ptr = new ComputePi();
    }
    else if (series_type.compare("RiemannIntegral")==0){

        //parse integral parameters
        std::string str_func, str_a, str_b;
        sstr >> str_func >> str_a >> str_b;

        //parse function
        double a = std::stod(str_a);
        double b = std::stod(str_b);

        std::function<double(double)> func;
        if (str_func.compare("cube") == 0){
            func = [](double x) {return x * x * x;};
        }
        else if (str_func.compare("sin") == 0){
            func = [](double x) {return sin(x);};
        }
        else if (str_func.compare("cos") == 0){
            func = [](double x) {return cos(x);};
        }
        else{
            std::cout << "Only 'cube', 'sin', and 'cos' are supported" << std::endl;
            return -1;
        }

        ptr = new RiemannIntegral(func, a, b);
    }
    else {
        std::cout << "Not Supported" << std::endl;
        return -1;
    }
    double a = ptr -> compute(100);

    
    std::cout << a << std::endl;

    // define the output frequency and maxiter
    int frequency = 5;
    int maxiter = 100;

    if (dumper_type.compare( "print") ==0){
        PrintSeries dumper(*ptr, frequency, maxiter );
        // set the precision of the output
        dumper.setPrecision(10);
        dumper.dump();
    }
    else if (dumper_type.compare( "write")==0){
        // specify the output file name 
        std::string file = "output_file.txt";
        WriteSeries dumper(*ptr, frequency, maxiter, file);
        // specify the output file type
        dumper.setSeparator(',');
        // set the precision of the output
        dumper.setPrecision(10);
        dumper.dump();
    }
    else {
        std::cout << "Not Supported" << std::endl;
        return -1;
    }

    // output dumper directly into a file 

    std::string filename = "output_file.txt";
    std::ofstream outputf (filename); 

    PrintSeries dumper(*ptr, frequency, maxiter);
    outputf << dumper << std::endl; 

    delete ptr;

    return 0;
}