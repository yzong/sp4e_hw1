# include "ComputePi.hh"
# include <math.h>
# include <iostream>

ComputePi::ComputePi(void){}
ComputePi::~ComputePi(void){}

double ComputePi::getAnalyticPrediction(void){
    return M_PI;
}

double ComputePi::computeTerm(unsigned int k){
    return double(1) / (k * k);
}

double ComputePi::compute(unsigned int N){
    return sqrt(6 * Series::compute(N));
}

