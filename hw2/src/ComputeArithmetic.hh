#ifndef COMPUTEARITHMETIC_HH
#define COMPUTEARITHMETIC_HH

# include "series.hh"

class ComputeArithmetic: public Series{

    public:
    ComputeArithmetic();
    virtual ~ComputeArithmetic();

    // double compute(unsigned int N);
    double getAnalyticPrediction(void) override;
    double computeTerm(unsigned int k) override;
};

#endif