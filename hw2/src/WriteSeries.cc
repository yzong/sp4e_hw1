#include "WriteSeries.hh"
#include <iostream>
#include <cmath>
#include <fstream>
#include <string>
#include <iomanip>

WriteSeries::WriteSeries(Series &series, int freq, int maxit, std::string &file): 

DumperSeries(series), 
maxiter(maxit), 
frequency(freq),
filename(file) 
{}
WriteSeries::~WriteSeries(){}

void WriteSeries::dump(std::ostream & os) {  
    
    std::ofstream outfile(filename);
    
    if (!outfile.is_open()) {
        std::cerr << "Error: Unable to open file " << filename << std::endl;
        return;
    }

    for (int i=1; i<maxiter/frequency; i++) {
        double res = series.compute(i*frequency);
        // get analytic prediction 
        double res_analytic = series.getAnalyticPrediction(); 
        // set precision in the output 
        outfile << std::setprecision(precision); 
        
        if (!std::isnan(res_analytic)) {
        outfile << i*frequency << separator << res << separator << res_analytic << "\n" <<std::endl;
        
        } 
        else {
        outfile << i*frequency << separator << res << separator << "\n" <<std::endl;
        }       
    }
    
    outfile.close();

}

void WriteSeries::setSeparator(char newSeparator) {
    char separator = newSeparator;
    // Update the file extension based on the separator
    if (separator == ',') {
        filename = filename.substr(0, filename.find_last_of(".")) + ".csv";
    } 
    else if (separator == '|') {
        filename = filename.substr(0, filename.find_last_of(".")) + ".psv";
    } 
    else {
        filename = filename.substr(0, filename.find_last_of(".")) + ".txt";
    }
}


