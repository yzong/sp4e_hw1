#include "PrintSeries.hh"
#include <iostream>
#include <cmath>
#include <iomanip>

// initialize the class, the attribute series is inherited
PrintSeries::PrintSeries(Series& series, int freq, int maxit) : 
DumperSeries(series), 
maxiter(maxit), 
frequency(freq) 
{}
PrintSeries::~PrintSeries(){}

void PrintSeries::dump(std::ostream & os ) {  
    
    for (int i=1; i<maxiter/frequency; i++) {

        double res = series.compute(i*frequency - 1);
        double res2 = series.compute(i*frequency);
        
        // get analytic prediction 
        double res_analytic = series.getAnalyticPrediction();
        // set precision 
        os << std::setprecision(precision); 
        // Check if the analytic prediction is not NaN before printing
        if (!std::isnan(res_analytic)) {    
        os << i*frequency << " " << res << " " << res2 - res << " " << res_analytic - res2 << "\n" <<std::endl;    
        } 
        else {   
        os << i*frequency << " " << res << " " << res2 - res  << " " << "\n" <<std::endl;
        }
    }
}