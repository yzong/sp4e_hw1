#ifndef PRINTSERIES_HH
#define PRINTSERIES_HH

#include "DumperSeries.hh"
#include <iostream>

class PrintSeries : public DumperSeries{
    public:
      
        PrintSeries(Series &series, int freq, int maxit);
        virtual ~PrintSeries();

        void dump(std::ostream & os = std::cout) override ;
    
    private:
        int frequency ;
        int maxiter;

};

  

#endif