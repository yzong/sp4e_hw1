#include "DumperSeries.hh"
#include <iostream>

DumperSeries::DumperSeries(Series &s) : series(s), precision(10) {} 
DumperSeries::~DumperSeries(){} 

void DumperSeries::setPrecision (unsigned int newprecision ) {

    this -> precision = newprecision;    
}

