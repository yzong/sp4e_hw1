#include "RiemannIntegral.hh"
#include <iostream>

RiemannIntegral::RiemannIntegral(std::function<double(double)> func, double a, double b): f(func), a(a), b(b){}
RiemannIntegral::~RiemannIntegral(void){}

double RiemannIntegral::compute(unsigned int N){
    this -> dx = (this -> b - this -> a) / N;
    double res = 0;
    for (int i=0; i<N; i++){
        res += this -> dx * f (this -> a + i * this -> dx);
    }
    return res;
}

double RiemannIntegral::computeTerm(unsigned int k){
    // dummy function to implement pure virtual function
    return 0;
}