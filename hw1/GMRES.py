import numpy as np

def Arnoldi (A, b, x0, n:int, tol=1e-6):
    '''
    Generate a orthonormal basis of (n + 1)-th Krylov subspace of matrix A
    This process is part of the GMRES method to solve the Ax=b problem.
    Based on the implementation in https://en.wikipedia.org/wiki/Arnoldi_iteration

    <Parameters>:
    A: (m, m) numpy.array
        A matrix in the Ax=b problem
    b: (m, ) numpy.array
        b vector in Ax=b problem
    x0: (m, ) numpy.array
        initial guess of the solution vector to the Ax=b problem
    n: int
        degree of Krylov space is (n + 1)
    
    <Returns>:
    Q: (m, n + 1) numpy.array
        orthonormal basis of (n + 1)-th Krylov space
    H: (n + 1, n) numpy.array
        upper Hessenberg matrix
    '''
    
    # initialize output matices
    Q = np.zeros((A.shape[0], n + 1))
    H = np.zeros((n + 1, n))

    # use normalized residual as initial vector
    r0 = b - A.dot(x0)
    Q[:, 0] = r0/np.linalg.norm(r0)

    for k in range(1, n + 1):
        Q[:, k] = np.einsum('ij, j', A, Q[:, k - 1])
        for j in range(k):
            H[j, k - 1] = np.einsum('i, i', Q[:, j], Q[:, k])
            Q[:, k] = Q[:, k] - H[j, k - 1] * Q[:, j]
        H[k, k - 1] = np.linalg.norm(Q[:, k],2)

        if H[k, k - 1] > tol:
            Q[:, k] = Q[:, k] / H[k, k - 1]
        else:
            return Q, H

def GMRES(A, b, x0, maxdim:int, maxit=100, tol=1e-6, callback=None):
    '''
    Numerical solver for Ax=b problem using GMRES method.

    <Parameters>:
    A: (m, m) numpy.array
        A matrix in the Ax=b problem
    b: (m, ) numpy.array
        b vector in Ax=b problem
    x0: (m, ) numpy.array
        initial guess of the solution vector to the Ax=b problem
    maxdim: int
        maximum dimension of Krylov subspace used to solve the problem
    maxit: int, default 100
        maximum number of iterations allowed
    tol: int, default 1e-6
        convergence threshold
    
    <Returns>:
    xn: (m, ) numpy.array
        solution vector
    res_list: list of float
        list of residual value at each iteration step
    '''

    # calculate initial residual with the guess x0
    res = np.linalg.norm((b - np.einsum('ij, j', A, x0)))

    it = 1
    res_list = [res]
    while (res > tol) & (it <= maxit):
        # Arnoldi iteration
        Q, H = Arnoldi(A, b, x0, maxdim)
        Qn = Q[:, :-1]
        Hn = H[:-1, :]

        # compute vector beta and e1 for the equivalent problem min(||Hn * yn - beta * e1||)
        beta = np.linalg.norm((b - np.einsum('ij, j', A, x0)))
        e1 = np.zeros(b.shape[0])
        e1[0] = 1

        # solve the equivalent problem min(||Hn * yn - beta * e1||)
        yn, _, _, _ = np.linalg.lstsq(Hn, beta * e1, rcond=None)

        # calculate new residual and solution vector
        res = np.linalg.norm(np.einsum('ij, j', Hn, yn) - beta * e1)
        res_list.append(res)
        xn = x0 + np.einsum('ij, j', Qn, yn)

        if callback != None:
            callback(xn)
        
        it += 1

    return xn, res_list