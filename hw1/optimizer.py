import scipy 
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import argparse

### Input arguments of the module

parser = argparse.ArgumentParser()
parser.add_argument('--A', nargs=4 , type = float,  help= 'coefficients of the quadratic term')
parser.add_argument('--b', nargs= 2, type= float, help= 'coefficients of the linear term ')
parser.add_argument('--x0', nargs='+', type = float, help="Initial guess of solution x0")
parser.add_argument('--optimizer', type=str, help = 'select the optimizer method') 
parser.add_argument('--plot', action='store_true', help = 'Surface plot')

args = parser.parse_args()

A = np.array([args.A[:2], args.A[2:]])
b = np.array(args.b)

def quadratic_fun(x):
    """
    Compute the value of a quadratic function.
    
    """
    s = 0.5 * np.dot(x.T, np.dot(A,x)) - np.dot(x.T,b)

    return s

def callback_fun (point):
    """
    Callback function to retrieve the coordinates of the optimization steps
    
    """
    points.append(point)
    return points

### Starting point of the optimization 
x0 = np.array(args.x0)
points = [x0]

# Minization of the quadratic function 
if args.optimizer == 'BFGS':
    res = scipy.optimize.minimize (quadratic_fun, x0 , method = 'BFGS', callback = callback_fun)

elif args.optimizer == 'LGMRES':
    res = scipy.sparse.linalg.lgmres(A,b, [0,0], callback = callback_fun)

else :
    raise AssertionError("Unexpected optimizer string !", args.optimizer)

print('Optimization points:', points)
print(res)


if args.plot:
    ps = np.array(points)

    x_point = np.arange (-10,10,0.5)
    y_point = np.arange(-10,10,0.5)
    x_point, y_point = np.meshgrid(x_point,y_point)

    # evaluate the function for 3D plotting
    z = 0.5 * (x_point * (A[0, 0] * x_point + A[0, 1] * y_point) + y_point * (A[1, 0] * x_point + A[1, 1] * y_point)) - (x_point * b[0] + y_point * b[1])

    ### Plot quadratic function and optimization steps
    fig, ax = plt.subplots()

    ax = plt.axes(projection='3d')
    ax.plot_surface(x_point, y_point, z, rstride=1, cstride=1, cmap =cm.coolwarm, edgecolor = 'none', alpha=.6)
    ax.contour(x_point, y_point, z)

    #Plotting the optmization points 
    ax.plot(ps[:,0], ps[:,1] , '--ro')

    # Caption of the figure
    ax.set_title("Surface plot", fontsize = 15)
    ax.set_xlabel('x', fontsize = 10)
    ax.set_ylabel('y', fontsize = 10)
    ax.set_zlabel('z', fontsize = 10)

    plt.show()



