import argparse
from GMRES import *
import scipy
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

parser = argparse.ArgumentParser(
    prog='GMRES',
    description='Use GMRES method to solve linear equation Ax=b',
    epilog='v2023-10-17'
)

parser.add_argument('--A', nargs='+', type=float, help="A matrix")
parser.add_argument('--b', nargs='+', help="b vector", type = float)
parser.add_argument('--x0', nargs='+', help="Initial guess of solution x0", type = float)
parser.add_argument('--maxdim', help="Maximum dimention of Krylov subspace, should be integer >=1", 
                    type=int, nargs='?', default=2)
parser.add_argument('--maxit', help="Maximum number of iterations allowed", 
                    type=int, nargs='?', default=100)
parser.add_argument('--tol', help="tolerance vaule of residual used to stop iteration", 
                    type=float, nargs='?', default=1e-6)
parser.add_argument('--optimizer', type=str, help = 'select the optimizer method', nargs='?', default='GMRES')
parser.add_argument('--plot', action='store_true', help = 'Surface plot')

args=parser.parse_args()


if not (len(args.A) == len(args.b)**2 == len(args.x0)**2):
    raise ValueError("Sizes of A ({0}), b ({1}), x0 ({2}) don't match."
                     .format(len(args.A), len(args.b), len(args.x0)))
if args.maxdim < 1:
    raise ValueError("maxdim should be integer larger than 1, not {0}.".format(args.maxdim))

if args.maxit < 1:
    raise ValueError("maxdim number of iterations allowed shoud be integer larger than 1, not {0}."
                     .format(args.maxit))
b = np.array(args.b)
x0 = np.array(args.x0)
A = np.array(args.A).reshape(len(args.b),-1)

def callback_fun (point):
    """
    Callback function to retrieve the coordinates of the optimization steps
    
    """
    points.append(point)
    return points

def quadratic_fun(x):
    """
    Compute the value of a quadratic function.
    
    """
    s = 0.5 * np.dot(x.T, np.dot(A, x)) - np.dot(x.T, b)

    return s

# Starting point of the optimization 
points = [x0]

if args.optimizer == 'GMRES':
    xn, _ = GMRES(A, b, x0, args.maxdim, args.maxit, args.tol, callback = callback_fun)
elif args.optimizer == 'BFGS':
    res = scipy.optimize.minimize (quadratic_fun, x0 , method = 'BFGS', callback = callback_fun)
    xn = res.x
elif args.optimizer == 'LGMRES':
    res = scipy.sparse.linalg.lgmres(A, b, x0, callback = callback_fun)
    xn = res[0]
else :
    raise AssertionError("Unexpected optimizer string !", args.optimizer)

if args.plot:
    ps = np.array(points)

    x_point = np.arange (-10,10,0.5)
    y_point = np.arange(-10,10,0.5)
    x_point, y_point = np.meshgrid(x_point,y_point)

    # evaluate the function for 3D plotting
    z = 0.5 * (x_point * (A[0, 0] * x_point + A[0, 1] * y_point) + y_point * (A[1, 0] * x_point + A[1, 1] * y_point)) - (x_point * b[0] + y_point * b[1])

    # Plot quadratic function and optimization steps
    fig, ax = plt.subplots()

    ax = plt.axes(projection='3d')
    ax.plot_surface(x_point, y_point, z, rstride=1, cstride=1, cmap =cm.coolwarm, edgecolor = 'none', alpha=.6)
    ax.contour(x_point, y_point, z)

    # Plotting the optmization points 
    ax.plot(ps[:,0], ps[:,1] , '--ro')

    # Caption of the figure
    ax.set_title("Surface plot", fontsize = 15)
    ax.set_xlabel('x', fontsize = 10)
    ax.set_ylabel('y', fontsize = 10)
    ax.set_zlabel('z', fontsize = 10)

    plt.savefig('{}.png'.format(args.optimizer), dpi=300)
    plt.show()

print('Solution: {}'.format(xn))
print('obtained after {0} iterations'.format(len(points) - 1))