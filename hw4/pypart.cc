#include <pybind11/pybind11.h>

namespace py = pybind11;

#include "csv_writer.hh"
#include "material_points_factory.hh"
#include "ping_pong_balls_factory.hh"
#include "planets_factory.hh"
#include "compute_interaction.hh"
#include "compute_gravity.hh"
#include "compute_verlet_integration.hh"
#include "compute_temperature.hh"


PYBIND11_MODULE(pypart, m) {

  m.doc() = "pybind of the Particles project";


  py::class_<ParticlesFactoryInterface>(m, "ParticlesFactoryInterface")
    // .def(py::init<>())
    .def("createSimulation", 
        //py::overload_cast<const std::string&, Real>(&ParticlesFactoryInterface::createSimulation),
        static_cast<SystemEvolution& (ParticlesFactoryInterface::*)(const std::string&, Real)>(&ParticlesFactoryInterface::createSimulation),
        py::arg("fname"), py::arg("timestep")
        )
    .def("createSimulation",
        //py::overload_cast<const std::string&, Real, std::function<void(Real)>>(&ParticlesFactoryInterface::createSimulation),
        static_cast<SystemEvolution& (ParticlesFactoryInterface::*)(const std::string&, Real, py::function )>(&ParticlesFactoryInterface::createSimulation),
        py::arg("fname"), py::arg("timestep"), py::arg("func")
        )
    .def_static("getInstance", &ParticlesFactoryInterface::getInstance, py::return_value_policy::reference)
    // .def("getSystemEvolution", &ParticlesFactoryInterface::getSystemEvolution) do we need to access this??
    .def_property_readonly("system_evolution", &ParticlesFactoryInterface::getSystemEvolution);

  py::class_<PlanetsFactory, ParticlesFactoryInterface>(m, "PlanetsFactory")
    //.def(py::init<>())
    .def("createDefaultComputes", &PlanetsFactory::createDefaultComputes)
    .def("createParticle", &PlanetsFactory::createParticle)
    .def_static("getInstance", &PlanetsFactory::getInstance, py::return_value_policy::reference);

  py::class_<PingPongBallsFactory, ParticlesFactoryInterface>(m, "PingPongBallsFactory")
    //.def(py::init<>())
    .def("createParticle", &PingPongBallsFactory::createParticle)
    .def_static("getInstance", &PingPongBallsFactory::getInstance, py::return_value_policy::reference);

  py::class_<MaterialPointsFactory, ParticlesFactoryInterface>(m, "MaterialPointsFactory")
    //.def(py::init<>())
    //.def("createSimulation", &MaterialPointsFactory::createSimulation, py::arg("fname"), py::arg("timestep"))
    .def("createDefaultComputes", &MaterialPointsFactory::createDefaultComputes, py::arg("timestep"))
    .def("createParticle", &MaterialPointsFactory::createParticle)
    .def_static("getInstance", &MaterialPointsFactory::getInstance, py::return_value_policy::reference);

  py::class_<Compute, std::shared_ptr<Compute>>(m, "Compute")
    // .def(py::init<>())
    .def("compute", &Compute::compute, py::arg("system"));
  
  py::class_<ComputeInteraction, Compute, std::shared_ptr<ComputeInteraction>>(m, "ComputeInteraction")
    // .def(py::init<>())
    .def("applyOnPairs", &ComputeInteraction::applyOnPairs<py::function>, py::arg("func"), py::arg("system"));

  py::class_<ComputeGravity, ComputeInteraction, std::shared_ptr<ComputeGravity>>(m, "ComputeGravity")
    .def(py::init<>())
    .def("compute", &ComputeGravity::compute, py::arg("system"))
    .def("setG", &ComputeGravity::setG)
    .def_property("G", nullptr, &ComputeGravity::setG);

  py::class_<ComputeTemperature, Compute, std::shared_ptr<ComputeTemperature>>(m, "ComputeTemperature")
    .def(py::init<>())
    .def("compute", &ComputeTemperature::compute)
    .def_property("conductivity", &ComputeTemperature::getConductivity, [](ComputeTemperature &t, Real &a) { return t.getConductivity()=a; }) 
    .def_property("capacity", &ComputeTemperature::getCapacity, [](ComputeTemperature &t, Real &a) { return t.getCapacity()=a; }) 
    .def_property("density", &ComputeTemperature::getDensity,[](ComputeTemperature &t, Real &a) { return t.getDensity()=a; })  
    .def_property("L", &ComputeTemperature::getL, [](ComputeTemperature &t, Real &a) { return t.getL()=a; } )  
    .def_property("delta_t", &ComputeTemperature::getDeltat, [](ComputeTemperature &t, Real &a) { return t.getDeltat()=a; });

  py::class_<ComputeVerletIntegration, Compute, std::shared_ptr<ComputeVerletIntegration>>(m, "ComputeVerletIntegration")
    .def(py::init<Real>(), py::arg("timestep"))
    .def("compute", &ComputeVerletIntegration::compute)
    .def("addInteraction", &ComputeVerletIntegration::addInteraction)
    .def_property("dt", nullptr, &ComputeVerletIntegration::setDeltaT);

  py::class_<CsvWriter, Compute, std::shared_ptr<CsvWriter>> (m, "CsvWriter")
    .def(py::init<const std::string &>(), py::arg("filename"))
    .def("write", &CsvWriter::write)
    .def("compute", &CsvWriter::compute);

  py::class_<SystemEvolution> (m, "SystemEvolution")
    .def(py::init<std::shared_ptr<System>>(), py::arg("system") )
    .def("evolve", &SystemEvolution::evolve)
    .def("addCompute", &SystemEvolution::addCompute)
    .def("setNSteps", &SystemEvolution::setNSteps )
    .def("setDumpFreq", &SystemEvolution::setDumpFreq)
    .def("getSystem", &SystemEvolution::getSystem, py::return_value_policy::reference);
  
  py::class_<System> (m, "System")
    .def(py::init<>());
}