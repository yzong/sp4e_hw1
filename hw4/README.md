# Homework 4
first part is implemented in `pypart.cc`, second part is implemented in `part2.ipynb`.

before running the notebook:
1. create a `build/` folder and compile the project in it.
2. create a `dumps/` folder inside the `build/` folder.
3. make sure `init.csv` and `trajectories/` are in the `build/` folder.
4. run the `main.py` file and the  `part2.ipynb` inside the `build/` folder.
