# SP4E Homework 1

## Authors
Luca Righetti and Yuan Zong

## How to run the code

The file `optimizer.py` contains the solution of the first exercise. 
You can launch it giving 5 input arguments : 

1. The A matrix of the quadratic function (4 float)
2. The b vector (2 float)
3. The starting point of the optimization (2 float)
4. Which optimizer you want the use, either BFGS or LGMRES, in form of a string 
5. Wheter to visualize the optimization wiht a surface plot or not

This is an example on how to run it with BFGS optimizer and with plotting the results:
```
    python optimizer.py --A 8 1 1 3 --b 2 4 --x0 0 0 --optimizer BFGS --plot 
```

The main file `main.py` provides access to the GMRES solver implemented in `GMRES.py`. The user can also specify other solvers implemented in `optimizer.py`. The `main.py` allows user to specify the problem with flatten A, b matrices and initial guess x0 from terminal. It will lauch the solver as long as the input dimensions are matched.

To see detailed description of arguments in `main.py`, write in terminal:
```
    python3 main.py -h
```

To use the GMRES solver implemented in GMRES.py, write in the terminal:
```
    python3 main.py --A 8 1 1 3 --b 2 4 --x0 0 0 --maxdim 2 --optimizer GMRES
```

# SP4E Homework 2
This document include answers to the exercises and basic description of the code.

## Exercise 2
Since this homework requires two base classes, we decide that each person implement one base class and everything based on that class.

Problem 2.2-2.5 are implemented as described in Exercise 5.2. Problem 2.6 is implemented in `main.cc` and used in Dumper class. 

## Exercise 3 

Only the option for printing or writing to a file is passed as an argument to the main. 
Here is an example for calculating the arithmetic series and writing it to a file: 
```
~/sp4e_hw1/hw2/build$ ./main Arithmetic write
```

 The parameters maxiter and frequency have to be specified in the file `main.cc`. The same applies to output file and to the chosen separator. 

## Exercise 4

The precision has to be set in the `main.cc` file. 
Output results are written into a file by default, no matter what is the choice expressed in the command line. However the instructions above for running the main ar still valid. 

## Exercise 5
Consider the dumper with frequency 1, maxiter N, the complexity is $O(\frac{N(N+1)}{2})=O(N^2)$.

The repository only includes updated implementation. The `main` program allows the user to specify the quantity to be calculated and ways to visualize the convergence. Here is an example of calculating Pi and print the steps in command line:
```
~/sp4e_hw1/hw2/build$ ./main Pi print
```

After modifying the Series class, the complexity is $O(N)$.

In the case of summing terms reversely, the best complexity I can achieve is $O(N)$.

## Exercise 6
The `main` program allows the user to:
* specify the function to be integrated `cube` ($x^3$), `sin` ($sin(x)$) and `cos` ($cos(x)$)
* specify the integration range $[a,b]$
Below is an example of integrating the cubic function on $[0,1]$ and print the convergence series in command line:
```
./main RiemannIntegral print cube 0 1
```

Value $N$ required to get 2 digit precision:
* cubic: 6000
* sin: 80
* cos: 320

